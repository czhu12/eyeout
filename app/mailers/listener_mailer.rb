class ListenerMailer < ActionMailer::Base
  include SendGrid

  def alert_email(listener)
    @course = "#{listener["dept"].upcase} #{listener["course"]} #{listener["section"]}"
    mail(:to => listener['email'],
         :subject => "#{@course} has seats open", :from=>"no-reply@aneyeout.com")
  end

  def inform_email(listener)
    @course = "#{listener["dept"].upcase} #{listener["course"]} #{listener["section"]}"
    mail(:to => listener['email'],
         :subject => "Checking for #{@course}", :from=>"no-reply@aneyeout.com")
  end
end
