class ApplicationMailer < ActionMailer::Base
  default from: 'noreply-aneyeout@aneyeout.com'
  layout 'mailer'
end
