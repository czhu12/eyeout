$(document).ready(function() {
  runScript();
});

function hasActiveClass(obj) {
  var isActive = false;
  if (obj.attr('class')) {
    var classes = obj.attr('class').split(/\s+/);
    classes.map(function(className) {
      if (className == "active") {
        isActive = true;
      }
    });
  }

  return isActive;
}

function runScript() {
  $('.chat-list').scrollTop($('.chat-list')[0].scrollHeight);

  $(".dropdown-menu li a").click(function(){
    $("#dropdownMenu1").text($(this).text());
    $("#dropdownMenu1").val($(this).data('value'));
  });

  $("#submit").click(function() {
    $("#submit").attr('disabled', true);
    var phone_number = $("#phone-number-field").val().trim();
    var dept = $("#dept-field").val().trim();
    var course = $("#course-field").val().trim();
    var section = $("#section-field").val().trim();
    var email = $("#email-field").val().trim();
    var year = 2015;
    var sessionVal = $("#dropdownMenu1").text();
    var session = 'W';

    var sttSeats = hasActiveClass($("#stt-seats"));
    var restrictedSeats = hasActiveClass($("#restricted-seats"));
    var generalSeats = hasActiveClass($("#general-seats"));

    if (dept === '' || course == '' || section == '') {
      $("#error-messages").html("You have to fill in the fields for your course.");
      $("#submit").attr('disabled', false);
      return;
    }

    if (email === '' && phone_number === '') {
      $("#error-messages").html("You have to give us a phone number or an email so we know who to contact when the course opens up.");
      $("#submit").attr('disabled', false);
      return;
    }

    if (sessionVal == "Summer") {
      session = 'S';
    } else {
      session = 'W';
    }

    var listener = {
      phone_number: phone_number,
      email: email,
      dept: dept,
      course: course,
      section:section,
      session: session,
      year: year,
      stt: sttSeats,
      general: generalSeats,
      restricted: restrictedSeats
    }

    $.post("/eyeout", {listener: listener}, function(data) {
      $("#submit").attr('disabled', false);
      if ("id" in data) {
        $("#error-messages").css('display', 'none');
        $("#error-messages").html('');
        window.location.replace('/eyeout/' + data.id);
      } else if ("error" in data) {
        $("#error-messages").css('display', 'block');
        $("#error-messages").html('Error, something failed.');
      } else if ("ratelimit" in data) {
        console.log('rate limit hit');
        $("#error-messages").css('display', 'block');
        $("#error-messages").html('Phone registrations are limited to 5 a day due to how expensive it is getting. Email registrations are unlimited though!');
      } else if ("message" in data) {
        // handle doesnt exist course
        var courseName = [dept, course, section, year + session].join(' ');
        $("#error-messages").css('display', 'block');
        $("#error-messages").html('It looks like ' + courseName + ' doesn\'t exist');
      }
    });
  });

  $("#chat-enter").click(function() {
    var body = $('#chat-message').val().trim();
    if (body) {
      $('#chat-message').val('');
      var message = {
        body: body
      };

      $.post("/messages", {message: message}, function(data) {
        $("#chat-messages").append("<li>" + body + "</li>");
      });
    }
  });
}
