require 'notifier'
require 'list_logger'

class ListenersController < ApplicationController
  before_filter :authenticate, :only =>[:admin]

  def new
    @listener_count = Listener.where('fulfilled=?', true).count
    @messages = Message.order(created_at: :desc).limit(100).reverse
  end

  def create
    @listener = Listener.new(listener_params) 
    #if @listener.phone_number && exceeded_rate_limit(@listener.phone_number)
    #  render :json=>{:ratelimit => "Rate limit exceeded"}
    #  return
    #end
    # This is where we validate that the course actually exists.
    if @listener.exists?
      if @listener.save
        render :json => @listener
        send_confirmation(@listener)
        @listener.immediate_check
      else
        render :json => { :error => @listener.errors}
      end
    else
      render :json => { :message => "Course does not exist" }
    end
  end

  def send_confirmation(listener)
    notifier = Notifier.new
    notifier.send_inform(listener)
  end

  def show
    @listener = Listener.find params[:id]
  end

  def admin
    @since_1 = Listener.where("created_at > ?", 1.hours.ago).count
    @since_2 = Listener.where("created_at > ?", 2.hours.ago).count
    @since_4 = Listener.where("created_at > ?", 4.hours.ago).count
    @since_8 = Listener.where("created_at > ?", 8.hours.ago).count
    @since_24 = Listener.where("created_at > ?", 24.hours.ago).count
    @finished_1 = Listener.where("updated_at > ? AND fulfilled=?", 1.hours.ago, true).count
    @finished_4 = Listener.where("updated_at > ? AND fulfilled=?", 4.hours.ago, true).count
    @finished_8 = Listener.where("updated_at > ? AND fulfilled=?", 8.hours.ago, true).count
    @finished_24 = Listener.where("updated_at > ? AND fulfilled=?", 24.hours.ago, true).count
    @emails_count_24 = Listener.where('email != ? AND created_at > ?', "", 24.hours.ago).count
    @phone_count_24 = Listener.where('phone_number != ? AND created_at > ?', "", 24.hours.ago).count
    @phone_uniques = Listener.select(:phone_number).distinct.count
    @email_uniques = Listener.select(:email).distinct.count
    @total = Listener.count
    @unfulfilled_count = Listener.where("fulfilled=?", false).count

    logger = ListLogger.new
    @cache_ratios = logger.get("cache_ratio")
    if @cache_ratios.count > 5
      @cache_ratios = @cache_ratios.from(@cache_ratios.count - 5)
    end

    @unique_courses = logger.get("cache_misses")
    if @unique_courses.count > 5
      @unique_courses = @unique_courses.from(@unique_courses.count - 5)
    end

    @total_time = logger.get("total_time")
    if @total_time.count > 5
      @total_time = @total_time.from(@total_time.count - 5)
    end

    @not_exist_count = logger.get("not_exist_count")
    if @not_exist_count.count > 5
      @not_exist_count = @not_exist_count.from(@not_exist_count.count - 5)
    end
  end

  private
  def listener_params
    params.require(:listener).permit!
  end

  def exceeded_rate_limit(phone_number)
    return Listener.where('phone_number=? AND created_at > ?', phone_number, 24.hours.ago).count > 4
  end
end
