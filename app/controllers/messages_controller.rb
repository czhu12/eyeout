class MessagesController < ApplicationController
  def create
    body = params[:message][:body]
    m = Message.new
    m.body = body

    if m.save
      render :json=>{:message=> "success"}
    else
      render :json=>{:message=> "failure"}
    end
  end
end
