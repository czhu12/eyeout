class Message < ActiveRecord::Base
  before_save :check_for_admin

  def check_for_admin
    if self.body.starts_with?("<chriszhu> ")
      self.admin = true
      self.body = self.body["<chriszhu> ".size, self.body.size]
    end
  end
end
