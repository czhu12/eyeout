require 'poller'
require 'course_api'

class Listener < ActiveRecord::Base
  validates_presence_of :dept
  validates_presence_of :course
  validates_presence_of :section
  validates_presence_of :year
  validate :has_phone_number_or_email

  def has_phone_number_or_email
    if not (self.phone_number || self.email)
      errors.add(:check_point_missing, "check point missing")
    end
  end

  def exists?
    not CourseAPI.get_course_api(self).nil?
  end

  def self.poll_all
    sql = "SELECT * FROM listeners WHERE fulfilled = 'f';"
    listeners = ActiveRecord::Base.connection.execute(sql)
    Poller.poll_all(listeners)
  end

  def route
    year = self.year
    dept = self.dept.upcase
    course = self.course.strip
    section = self.section.strip
    session = self.session.strip
    base_route = "https://courses.students.ubc.ca/cs/servlets/SRVCourseSchedule?sessyr=#{year}&sesscd=#{session}&req=5&dept=#{dept}&course=#{course}&section=#{section}&output=3"
    base_route
  end

  def self.found(listener_object)
    listener = Listener.find listener_object["id"]
    listener.update_attributes(:fulfilled => true)
  end

  def self.doesnt_exist?(listener_object)
    listener = Listener.find listener_object["id"]
    failure_count = listener.failure_count
    return true if failure_count >= 3

    listener.update_attributes(:failure_count => failure_count + 1)
    false
  end

  def course_name
    return "#{dept.upcase} #{course} #{section}"
  end

  def immediate_check
    CourseAPI.notify_if_empty(self)
  end

end
