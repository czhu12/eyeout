Rails.application.routes.draw do
  get 'sessions/new'

  post 'messages', to: 'messages#create', via:[:post]

  match 'eyeout/new', to: 'listeners#new', via:[:get]
  match 'eyeout', to: 'listeners#create', via:[:post]
  match 'eyeout/:id', to: 'listeners#show', via:[:get]
  get "log_out" => "sessions#destroy", :as => "log_out"
  get "log_in" => "sessions#new", :as => "log_in"
  resources :sessions

  get "admin" => "listeners#admin"
  
  root 'listeners#new'
end
