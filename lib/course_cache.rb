class CourseCache
  def initialize
    @valid_statuses = ["STT", "Full", " ", "Restricted"]
    reset
  end

  def reset
    @cache = {}
  end

  def put(listener, status)
    if @valid_statuses.include?(status)
      key = key_for(listener)
      @cache[key] = status
    end

    return nil
  end

  def get(listener)
    if contains?(listener)
      key = key_for(listener)
      return @cache[key]
    end

    return nil
  end

  def contains?(listener)
    key = key_for(listener)
    return @cache.has_key?(key)
  end

  def key_for(listener)
    year = listener["year"]
    dept = listener["dept"].upcase
    course = listener["course"].strip
    section = listener["section"].strip
    session = listener["session"].strip

    return "#{dept}-#{course}-#{section}-#{session}-#{year}"
  end

  def dump
    @cache.each do |course, status|
      puts "#{course}  #{status}"
    end
  end
end
