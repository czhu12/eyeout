require 'json'

class ListLogger
  def put(key, value)
    Redis.current.with { |conn| 
      log_serialized = conn.get key
      if log_serialized.nil?
        log_for_key = []
      else
        log_for_key = JSON.parse(log_serialized)
      end

      log_for_key.push(value)
      conn.set key, log_for_key.to_json
    }
  end

  def get(key)
    Redis.current.with { |conn|
      value = conn.get key
      if value.nil?
        return []
      else
        return JSON.parse(value)
      end
    }
  end
end
