require 'net/http'
require 'open-uri'
class CourseAPI
  def self.get_route(listener)
    year = listener["year"]
    dept = listener["dept"].upcase
    course = listener["course"].strip
    section = listener["section"].strip
    session = listener["session"].strip

    base_route = "https://courses.students.ubc.ca/cs/servlets/SRVCourseSchedule?sessyr=#{year}&sesscd=#{session}&req=5&dept=#{dept}&course=#{course}&section=#{section}&output=3"
    base_route
  end

  def self.get_course_api(listener)
    route = CourseAPI.get_route(listener)
    puts route
    
    xml = Nokogiri::XML(open(route))
    section = xml.xpath("//sections/section").first

    if section.nil?
      return nil
    end
    return section
  end

  def self.notify_if_empty(listener)
    route = CourseAPI.get_route(listener)
    xml = Nokogiri::XML(open(route))
    
    section = xml.xpath("//sections/section").first
    if section.nil?
      return
    end

    status = section.attributes["status"].value
    CourseAPI.notify(listener, status)
  end

  def self.notify(listener, status)
    notifier = Notifier.new
    if status == 'Restricted' and listener['restricted'] == 't'
      notifier.notify listener
      Listener.found listener
    elsif status == "STT" and listener['stt'] == 't'
      notifier.notify listener
      Listener.found listener
    elsif status == " " 
      notifier.notify listener
      Listener.found listener
    end
  end
end
