require 'twilio-ruby' 

class Notifier
  def initialize
    @account_sid = 'ACf0b3ee03335b85914e312db09f876bfe' 
    @auth_token = 'ee75f92da0f8e779e8f8604884a33380' 
    @client = Twilio::REST::Client.new @account_sid, @auth_token 
  end

  def send_inform(listener)
    #if listener.phone_number != ""
    #  listener_phone_number = listener.phone_number.gsub(/\s+/, "")
    #  course_name = listener.course_name
    #  @client.account.messages.create({
    #    :from => '+17786554178', 
    #    :to => listener_phone_number, 
    #    :body => "We are currently checking for #{course_name}. We will text you when we find a spot open for you!",  
    #  })
    #end
    
    if listener.email != ""
      ListenerMailer.inform_email(listener).deliver
    end
  end

  def send_text(listener)
    listener_phone_number = listener["phone_number"].gsub(/\s+/, "")
    @client.account.messages.create({
      :from => '+17786554178', 
      :to => listener_phone_number, 
      :body => get_body(listener),  
    })
    puts "notification sent to #{listener["phone_number"]} for #{get_course_name(listener)}"
  end

  def send_email(listener)
    ListenerMailer.alert_email(listener).deliver
    puts "notification sent to #{listener["email"]} for #{get_course_name(listener)}"
  end

  def notify(listener)
    if listener["phone_number"] != ""
      send_text(listener)
    end

    if listener["email"] != ""
      send_email(listener)
    end
  end

  def incorrect(listener)
    listener_phone_number = listener["phone_number"].gsub(/\s+/, "")
    @client.account.messages.create({
      :from => '+17786554178', 
      :to => listener_phone_number, 
      :body => get_body_incorrect(listener),  
    })
  end

  def whoops(listener)
    message = "There was some kind of error with the course checking by aneyeout. If you got a text saying that your course doesn't exist, ignore it. Your course is still full and we are still checking."

    puts 'whoops message sent'
    listener_phone_number = listener["phone_number"].gsub(/\s+/, "")
    @client.account.messages.create({
      :from => '+17786554178', 
      :to => listener_phone_number, 
      :body => message,  
    })
  end

  private
  def get_body_incorrect(listener) 
    course_name = get_course_name_extra(listener)
    return "#{course_name} is not a course. This service doesnt work for tutorials yet."
  end

  def get_body(listener)
    course_name = get_course_name(listener)
    random = rand(4)
    case random
    when 0
      return "#{course_name} has an open spot. Better hurry! Like us on Facebook if we helped at http://on.fb.me/1IWpap4"
    when 1
      return "Hey good lookin, theres a spot for you in #{course_name}. Like us on Facebook if we helped at http://on.fb.me/1IWpap4"
    when 2
      return "We found a spot for you in #{course_name}. Just for you. Like us on Facebook if we helped at http://on.fb.me/1IWpap4"
    else
      return "Theres a seat waiting for you in #{course_name}. Your welcome. Like us on Facebook if we helped at http://on.fb.me/1IWpap4"
    end
  end

  def get_course_name(listener)
    "#{listener["dept"]} #{listener["course"]} #{listener["section"]}"
  end

  def get_course_name_extra(listener)
    "#{listener["dept"]} #{listener["course"]} #{listener["section"]} #{listener["session"]}"
  end
end
