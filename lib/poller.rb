require 'thread'
require 'notifier'
require 'course_cache'
require 'net/http'
require 'open-uri'
require 'list_logger'

class Poller
  def self.poll_all(listeners)
    @list_logger = ListLogger.new
    @cache_misses = 0
    @cache_hits = 0
    @not_exist = 0
    @finished = 0
    @total = listeners.count
    start = Time.now
    notifier = Notifier.new
    @cache = CourseCache.new
    @queue = Queue.new

    #listeners.each do |listener|
    #  @queue.push(listener)
    #end

    listeners.each do |listener|
      begin
        Poller.poll(listener, notifier)
      rescue
        puts "Crashed. Probably wrong phone number"
      end
      @finished = @finished + 1
      puts "#{@finished} / #{@total}"
    end
    finish = Time.now
    @list_logger.put('cache_hits', @cache_hits)
    @list_logger.put('cache_misses', @cache_misses)
    @list_logger.put('cache_ratio', @cache_hits.to_f / (@cache_misses + @cache_hits))
    @list_logger.put('total_time', finish - start)
    @list_logger.put('not_exist_count', @not_exist)
    puts "Doesnt exist: #{@not_exist}"
    puts "Total time: #{finish - start}"
    @cache.reset
  end

  def self.poll(listener, notifier)
    if not @cache.contains?(listener)
      # Cache miss
      puts "cache MISS"
      @cache_misses = @cache_misses + 1
      sleep(0.4.seconds)
      route = Poller.get_route(listener)
      xml = Nokogiri::XML(open(route))
      
      section = xml.xpath("//sections/section").first
      if section.nil?
        puts "#{listener['dept']} #{listener['course']} #{listener['section']} does not exist"
        #if Listener.doesnt_exist?(listener)
        #  puts "Listener Disregarded"
        #  notifier.incorrect(listener)
        #  Listener.found listener
        #end
        #puts "listener failure incremented"

        @not_exist = @not_exist + 1
        return
      end

      status = section.attributes["status"].value
      @cache.put(listener, status)
    else
      # Cache hit
      puts "cache HIT"
      @cache_hits = @cache_hits + 1
      status = @cache.get(listener)
    end

    puts "#{listener["dept"]} #{listener["course"]} is #{status}"

    if status == 'Restricted' and listener['restricted'] == 't'
      notifier.notify listener
      Listener.found listener
    elsif status == "STT" and listener['stt'] == 't'
      notifier.notify listener
      Listener.found listener
    elsif status == " " 
      notifier.notify listener
      Listener.found listener
    end
  end

  def self.get_route(listener)
    year = listener["year"]
    dept = listener["dept"].upcase
    course = listener["course"].strip
    section = listener["section"].strip
    session = listener["session"].strip

    base_route = "https://courses.students.ubc.ca/cs/servlets/SRVCourseSchedule?sessyr=#{year}&sesscd=#{session}&req=5&dept=#{dept}&course=#{course}&section=#{section}&output=3"
    base_route
  end
end
