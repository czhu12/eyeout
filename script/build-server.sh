# Set up nginx

rm /etc/nginx/sites-enabled/*
ln -s /usr/src/app/config/nginx.conf /etc/nginx/sites-enabled/app.conf
service nginx start

# Nginx now configured and started...

cd /usr/src/app
rake assets:precompile
rake db:setup
rake db:migrate
rails s
