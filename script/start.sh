build() {
  docker build -t chriszhu/eyeout .
}

db() {
  docker run --name eyeout_db -e POSTGRES_PASSWORD=2eye4out8 -d postgres
}

app() {
  docker run -p 80:80 -i --link eyeout_db:postgres --name eyeout_app -d chriszhu/eyeout
}

clean() {
  docker rm -f eyeout_app
}

deploy() {
  build
  clean
  db
  app
}

action=$1
${action}
