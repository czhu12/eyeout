# Base image with ruby 2.2.0
FROM ruby:2.2.0

# Install required libraries and dependencies
RUN apt-get update && apt-get install -qy nodejs postgresql-client sqlite3 nginx --no-install-recommends && rm -rf /var/lib/apt/lists/*

# Set Rails version
ENV RAILS_VERSION 4.1.1

# Install Rails
RUN gem install rails --version "$RAILS_VERSION"

# Create directory from where the code will run 
RUN mkdir -p /usr/src/app  
WORKDIR /usr/src/app

# Make webserver reachable to the outside world
EXPOSE 80

# Set ENV variables
ENV PORT=3000
ENV RAILS_ENV=production
ENV SECRET_KEY_BASE=c6815674093a3ae7e87aeafb9f2cc25fb99bd26a16f79ece06c98fd6ff787a5fbd616b032ec8e59c0640dbba63002fc60e6abbe8c501cbab7a0d765ef573e085

# Install the necessary gems 
ADD Gemfile /usr/src/app/Gemfile  
ADD Gemfile.lock /usr/src/app/Gemfile.lock  

RUN bundle install --without development test

# Add rails project (from same dir as Dockerfile) to project directory
ADD ./ /usr/src/app

# Run rake tasks
ENTRYPOINT script/build-server.sh
