class AddSessionToListener < ActiveRecord::Migration
  def change
    add_column :listeners, :session, :string
  end
end
