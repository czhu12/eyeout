class AddAdminToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :admin, :boolean, default: false
  end
end
