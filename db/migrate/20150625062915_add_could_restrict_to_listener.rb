class AddCouldRestrictToListener < ActiveRecord::Migration
  def change
    add_column :listeners, :could_restrict, :boolean, default: false
  end
end
