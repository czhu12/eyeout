class CreateListeners < ActiveRecord::Migration
  def change
    create_table :listeners do |t|
      t.string :phone_number
      t.string :dept
      t.string :course
      t.string :section
      t.string :year

      t.timestamps
    end
  end
end
