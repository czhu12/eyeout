class AddFailureCountToListeners < ActiveRecord::Migration
  def change
    add_column :listeners, :failure_count, :integer, :default => 0
  end
end
