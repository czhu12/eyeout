class AddDifferentSeatsToListener < ActiveRecord::Migration
  def change
    add_column :listeners, :stt, :boolean, default: true
    add_column :listeners, :general, :boolean, default: true
    add_column :listeners, :restricted, :boolean, default: true
  end
end
