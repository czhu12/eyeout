class AddInformedToListener < ActiveRecord::Migration
  def change
    add_column :listeners, :informed, :boolean
  end
end
