class AddEmailToListener < ActiveRecord::Migration
  def change
    add_column :listeners, :email, :string
  end
end
