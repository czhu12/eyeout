class AddFulfilledToListeners < ActiveRecord::Migration
  def change
    add_column :listeners, :fulfilled, :boolean, :default => false
  end
end
